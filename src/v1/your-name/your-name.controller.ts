import { Controller, Get, Param } from '@nestjs/common';

@Controller('v1')
export class YourNameController {
  @Get('your-name/:frontName/:lastName')
  async getYourName(
    @Param('frontName') frontName: string,
    @Param('lastName') lastName: string,
  ) {
    return {
      front_name: frontName,
      last_name: lastName,
    };
  }
}
