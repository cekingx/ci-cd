import { Test, TestingModule } from '@nestjs/testing';
import { YourNameController } from './your-name.controller';

describe('YourNameController', () => {
  let controller: YourNameController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [YourNameController],
    }).compile();

    controller = module.get<YourNameController>(YourNameController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
