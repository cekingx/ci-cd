import { Module } from '@nestjs/common';
import { YourNameController } from './your-name/your-name.controller';

@Module({
  controllers: [YourNameController]
})
export class V1Module {}
